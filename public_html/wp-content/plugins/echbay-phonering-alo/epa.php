<?php
/**
* Plugin Name: EchBay Phonering Alo
* Description: A very simple yet very effective plugin that adds a Call Now button to your website for every device (mobile, table and desktop).
* Plugin URI: https://www.facebook.com/groups/wordpresseb
* Plugin Facebook page: https://www.facebook.com/webgiare.org
* Author: Dao Quoc Dai
* Author URI: https://www.facebook.com/ech.bay/
* Version: 1.0.6
* Text Domain: echbayepa
* Domain Path: /languages/
* License: GPLv2 or later
*/
if (! defined ( 'ABSPATH' )) {
exit ();
}
define ( 'EPA_DF_VERSION', '1.0.6' );
define ( 'EPA_DF_DIR', dirname ( __FILE__ ) . '/' );
define ( 'EPA_THIS_PLUGIN_NAME', 'EchBay Phonering Alo' );
if ( ! defined ( 'EBP_GLOBAL_PLUGINS_SLUG_NAME' ) ) {
define ( 'EBP_GLOBAL_PLUGINS_SLUG_NAME', 'echbay-plugins-menu' );
define ( 'EBP_GLOBAL_PLUGINS_MENU_NAME', 'Webgiare Plugins' );
define ( 'EPA_ADD_TO_SUB_MENU', false );
}
else {
define ( 'EPA_ADD_TO_SUB_MENU', true );
}
/*
* class.php
*/
if (! class_exists ( 'EPA_Actions_Module' )) {
class EPA_Actions_Module {
/*
* config
*/
var $default_setting = array (
'license' => '',
'hide_powered' => 1,
'widget_width' => 45,
'mobile_width' => 0,
'header_bg' => '#0084FF',
'widget_position' => 'bl',
'custom_style' => '/* Custom CSS */',
'phone_number' => ''
);
var $custom_setting = array ();
var $eb_plugin_media_version = EPA_DF_VERSION;
var $eb_plugin_prefix_option = '___epa___';
var $eb_plugin_root_dir = '';
var $eb_plugin_url = '';
var $eb_plugin_nonce = '';
var $eb_plugin_admin_dir = 'wp-admin';
var $web_link = '';
/*
* begin
*/
function load() {
/*
* test in localhost
*/
/*
if ( $_SERVER['HTTP_HOST'] == 'localhost:8888' ) {
$this->eb_plugin_media_version = time();
}
*/
/*
* Check and set config value
*/
$this->eb_plugin_root_dir = basename ( EPA_DF_DIR );
$this->eb_plugin_media_version = filemtime( EPA_DF_DIR . 'style.css' );
$this->eb_plugin_url = plugins_url () . '/' . $this->eb_plugin_root_dir . '/';
$this->eb_plugin_nonce = $this->eb_plugin_root_dir . EPA_DF_VERSION;
if ( defined ( 'WP_ADMIN_DIR' ) ) {
$this->eb_plugin_admin_dir = WP_ADMIN_DIR;
}
/*
* Load custom value
*/
$this->get_op ();
}
function get_op() {
global $wpdb;
$pref = $this->eb_plugin_prefix_option;
$sql = $wpdb->get_results ( "SELECT option_name, option_value
FROM
`" . $wpdb->options . "`
WHERE
option_name LIKE '{$pref}%'
ORDER BY
option_id", OBJECT );
foreach ( $sql as $v ) {
$this->custom_setting [str_replace ( $this->eb_plugin_prefix_option, '', $v->option_name )] = $v->option_value;
}
/*
* https://codex.wordpress.org/Validating_Sanitizing_and_Escaping_User_Data
*/
foreach ( $this->default_setting as $k => $v ) {
if (! isset ( $this->custom_setting [$k] )
|| $this->custom_setting [$k] == '' ) {
$this->custom_setting [$k] = $v;
}
}
foreach ( $this->custom_setting as $k => $v ) {
if ( $k == 'custom_style' ) {
$v = esc_textarea( $v );
}
else {
$v = esc_html( $v );
}
$this->custom_setting [$k] = $v;
}
}
function ck($v1, $v2, $e = ' checked') {
if ($v1 == $v2) {
return $e;
}
return '';
}
function get_web_link () {
if ( $this->web_link != '' ) {
return $this->web_link;
}
if ( defined('WP_SITEURL') ) {
$this->web_link = WP_SITEURL;
}
else if ( defined('WP_HOME') ) {
$this->web_link = WP_HOME;
}
else {
$this->web_link = get_option ( 'siteurl' );
}
$this->web_link = explode( '/', $this->web_link );
$this->web_link[2] = $_SERVER['HTTP_HOST'];
$this->web_link = implode( '/', $this->web_link );
if ( substr( $this->web_link, -1 ) == '/' ) {
$this->web_link = substr( $this->web_link, 0, -1 );
}
return $this->web_link;
}
function update() {
if ($_SERVER ['REQUEST_METHOD'] == 'POST' && isset( $_POST['_ebnonce'] )) {
if( ! wp_verify_nonce( $_POST['_ebnonce'], $this->eb_plugin_nonce ) ) {
wp_die('404 not found!');
}
foreach ( $_POST as $k => $v ) {
if (substr ( $k, 0, 5 ) == '_epa_') {
$key = $this->eb_plugin_prefix_option . substr ( $k, 5 );
delete_option ( $key );
if ( $k == '_epa_widget_width'
|| $k == '_epa_mobile_width' ) {
$v = (int) $v;
}
else {
$v = stripslashes ( stripslashes ( stripslashes ( $v ) ) );
$v = strip_tags( $v );
$v = sanitize_text_field( $v );
}
add_option( $key, $v, '', 'no' );
}
}
die ( '<script type="text/javascript">
alert("Update done!");
</script>' );
} // end if POST
}
function admin() {
$arr_position = array (
"tr" => 'Top Right',
"tl" => 'Top Left',
"cr" => 'Center Right',
"cl" => 'Center Left',
"br" => 'Bottom Right',
"bl" => 'Bottom Left'
);
$str_position = '';
foreach ( $arr_position as $k => $v ) {
$str_position .= '<option value="' . $k . '"' . $this->ck ( $this->custom_setting ['widget_position'], $k, ' selected' ) . '>' . $v . '</option>';
}
$arr_mobile_width = array (
"0" => 'All device (Desktop, Table, Mobile)',
"775" => 'Table and Mobile',
"455" => 'Mobile'
);
$str_mobile_width = '';
foreach ( $arr_mobile_width as $k => $v ) {
$str_mobile_width .= '<option value="' . $k . '"' . $this->ck ( $this->custom_setting ['mobile_width'], $k, ' selected' ) . '>' . $v . '</option>';
}
$this->eb_plugin_media_version = time();
$this->get_web_link();
$main = file_get_contents ( EPA_DF_DIR . 'admin.html', 1 );
$main = $this->template ( $main, $this->custom_setting );
$main = $this->template ( $main, $this->default_setting, 'aaa' );
$main = $this->template ( $main, array (
'_ebnonce' => wp_create_nonce( $this->eb_plugin_nonce ),
'str_position' => $str_position,
'str_mobile_width' => $str_mobile_width,
'epa_plugin_url' => $this->eb_plugin_url,
'epa_plugin_version' => $this->eb_plugin_media_version,
) );
echo $main;
echo '<p>* Other <a href="' . $this->web_link . '/' . $this->eb_plugin_admin_dir . '/plugin-install.php?s=itvn9online&tab=search&type=author" target="_blank">WordPress Plugins</a> written by the same author. Thanks for choose us!</p>';
}
function deline ( $str, $reg = "/\r\n|\n\r|\n|\r|\t/i", $re = "" ) {
$a = explode( "\n", $str );
$str = '';
foreach ( $a as $v ) {
$v = trim( $v );
if ( $v != '' ) {
if ( strstr( $v, '//' ) == true ) {
$v .= "\n";
}
$str .= $v;
}
}
return $str;
return preg_replace( $reg, $re, $str );
}
function guest() {
$max_center = $this->custom_setting ['widget_width'];
$max_width_btn = $max_center * 3;
$medium_width_btn = $max_center * 2;
$medium_center = $max_center / 2;
$epa_custom_css = '
.phonering-alo-phone.phonering-alo-green .phonering-alo-ph-img-circle{background-color: ' . $this->custom_setting ['header_bg'] . '}
/*
.phonering-alo-phone.phonering-alo-green .phonering-alo-ph-img-circle a{color: ' . $this->custom_setting ['header_bg'] . '}
*/
.phonering-alo-ph-img-circle {
width: ' . $this->custom_setting ['widget_width'] . 'px;
height: ' . $this->custom_setting ['widget_width'] . 'px;
top: ' . $max_center . 'px;
left: ' . $max_center . 'px;
}
.phonering-alo-ph-img-circle a {
width: ' . $this->custom_setting ['widget_width'] . 'px;
line-height: ' . $this->custom_setting ['widget_width'] . 'px;
}
.phonering-alo-ph-circle-fill {
width: ' . $medium_width_btn . 'px;
height: ' . $medium_width_btn . 'px;
top: ' . $medium_center . 'px;
left: ' . $medium_center . 'px;
}
.echbay-alo-phone,
.phonering-alo-ph-circle {
width: ' . $max_width_btn . 'px;
height: ' . $max_width_btn . 'px;
}
.style-for-position-cr,
.style-for-position-cl { margin-top: -' . ( $max_width_btn/ 2 ) . 'px; }
/* for mobile */
.style-for-mobile .style-for-position-bl {
left: -' . $max_center . 'px;
bottom: -' . $max_center . 'px;
}
.style-for-mobile .style-for-position-br {
right: -' . $max_center . 'px;
bottom: -' . $max_center . 'px;
}
.style-for-mobile .style-for-position-cl { left: -' . $max_center . 'px; }
.style-for-mobile .style-for-position-cr { right: -' . $max_center . 'px; }
.style-for-mobile .style-for-position-tl {
top: -' . $max_center . 'px;
left: -' . $max_center . 'px;
}
.style-for-mobile .style-for-position-tr {
top: -' . $max_center . 'px;
right: -' . $max_center . 'px;
}
@media screen and (max-width:' . $this->custom_setting ['mobile_width'] . 'px) {
.style-for-position-bl {
left: -' . $max_center . 'px;
bottom: -' . $max_center . 'px;
}
.style-for-position-br {
right: -' . $max_center . 'px;
bottom: -' . $max_center . 'px;
}
.style-for-position-cl { left: -' . $max_center . 'px; }
.style-for-position-cr { right: -' . $max_center . 'px; }
.style-for-position-tl {
top: -' . $max_center . 'px;
left: -' . $max_center . 'px;
}
.style-for-position-tr {
top: -' . $max_center . 'px;
right: -' . $max_center . 'px;
}
}
';
if ( $this->custom_setting ['mobile_width'] > 0 ) {
$epa_custom_css .= '
@media screen and (max-width:' . $this->custom_setting ['mobile_width'] . 'px) {
.echbay-alo-phone { display: block !important; }
}
';
}
else {
$epa_custom_css .= '
.echbay-alo-phone { display: block !important; }
';
}
$epa_custom_css = $this->deline( trim ( $epa_custom_css ) );
$epa_custom_css = str_replace( ';}', '}', $epa_custom_css );
$epa_custom_css .= trim ( $this->custom_setting ['custom_style'] );
$main = file_get_contents ( EPA_DF_DIR . 'guest.html', 1 );
$main = $this->template ( $main, $this->custom_setting + array (
'bloginfo_name' => get_bloginfo( 'name' ),
'epa_custom_css' => '<style type="text/css">' . $epa_custom_css . '</style>',
'epa_plugin_url' => $this->eb_plugin_url,
'epa_plugin_version' => $this->eb_plugin_media_version,
) );
echo $main;
}
function template($temp, $val = array(), $tmp = 'tmp') {
foreach ( $val as $k => $v ) {
$temp = str_replace ( '{' . $tmp . '.' . $k . '}', $v, $temp );
}
return $temp;
}
} // end my class
} // end check class exist
/*
* Show in admin
*/
function EPA_show_setting_form_in_admin() {
global $EPA_func;
$EPA_func->update ();
$EPA_func->admin ();
}
function EPA_add_menu_setting_to_admin_menu() {
if ( ! current_user_can('manage_options') )  {
return false;
}
$a = EPA_THIS_PLUGIN_NAME;
if ( EPA_ADD_TO_SUB_MENU == false ) {
add_menu_page( $a, EBP_GLOBAL_PLUGINS_MENU_NAME, 'manage_options', EBP_GLOBAL_PLUGINS_SLUG_NAME, 'EPA_show_setting_form_in_admin', NULL, 99 );
}
add_submenu_page( EBP_GLOBAL_PLUGINS_SLUG_NAME, $a, trim( str_replace( 'EchBay', '', $a ) ), 'manage_options', strtolower( str_replace( ' ', '-', $a ) ), 'EPA_show_setting_form_in_admin' );
}
/*
* Show in theme
*/
function EPA_show_facebook_messenger_box_in_site() {
global $EPA_func;
$EPA_func->guest ();
}
function EPA_plugin_settings_link ($links) {
$settings_link = '<a href="admin.php?page=' . strtolower( str_replace( ' ', '-', EPA_THIS_PLUGIN_NAME ) ) . '">Settings</a>';
array_unshift($links, $settings_link);
return $links;
}
$EPA_func = new EPA_Actions_Module ();
$EPA_func->load ();
if (is_admin ()) {
add_action ( 'admin_menu', 'EPA_add_menu_setting_to_admin_menu' );
if ( strstr( $_SERVER['REQUEST_URI'], 'plugins.php' ) == true ) {
$plugin = plugin_basename(__FILE__);
add_filter("plugin_action_links_$plugin", 'EPA_plugin_settings_link' );
}
}
else {
add_action ( 'wp_footer', 'EPA_show_facebook_messenger_box_in_site' );
}