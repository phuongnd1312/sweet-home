=== EchBay Phonering Alo ===
Plugin Name: EchBay Phonering Alo
Plugin URI: https://www.facebook.com/groups/wordpresseb
Plugin Facebook page: https://www.facebook.com/webgiare.org
Author: Dao Quoc Dai
Author URI: https://www.facebook.com/ech.bay
Text Domain: echbayepa
Tags: phonering alo, phone icon, easily button call, button call, sales
Requires at least: 3.3
Tested up to: 4.9
Stable tag: 1.0.6
Version: 1.0.6
Contributors: itvn9online
Donate link: https://paypal.me/itvn9online/5

== Description ==

Add Phonering Alo button to your website. A very simple yet very effective plugin that adds a Call Now button to your website for every device (mobile, table and desktop).

== Installation ==
1. Upload `echbay-phonering-alo` folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the `Plugins` admin menu in WordPress
3. Go to menu `Webgiare Plugins` menu and `Phonering Alo` menu in WordPress for custom setting.

== Frequently Asked Questions ==

== Screenshots ==
1. Phonering Alo display.
2. Phonering Alo setting in admin.

== Changelog ==

= Version 1.0.6 =
* position center for ring phone button

= Version 1.0.5 =
* Update style for pc

= Version 1.0.4 =
* Up for WP 4.9

= Version 1.0.3 =
* Custom select device for active plugin

= Version 1.0.2 =
* Fixed dafault value if zero

= Version 1.0.1 =
* Show or hidden button phone alo in desktop

= Version 1.0.0 =
* None

== Upgrade Notice ==

= Version 1.0.6 =
* None

= Version 1.0.5 =
* None

= Version 1.0.4 =
* None

= Version 1.0.3 =
* None

= Version 1.0.2 =
* None

= Version 1.0.1 =
* None

= Version 1.0.0 =
* None
